import copy
import numpy as np
import time
import mindspore as ms
import mindspore.numpy as msnp
from mindspore import nn
from mindspore import Tensor
from mindspore.ops import operations as P
from mindspore.ops import functional as F
from mindspore.ops import composite as C
from mindspore import dataset as ds
from mindspore.train import Model
from mindspore import context
from mindspore.common.initializer import initializer
from mindspore import ms_function

class ReducedSpace2D(nn.Cell):
    def __init__(self,
        net,
    ):
        super().__init__()

        self.net = net

        self.grad = C.GradOperation()

    def construct(self,R,V):
        # [B,A,3] -> [B,1]
        r = self.net(R)
        # [B,A,3]
        dr = self.grad(self.net)(R)
        # [B,1]
        batch_size = R.shape[0]
        v = C.batch_dot(dr.reshape(batch_size,-1),V.reshape(batch_size,-1))

        return r,v

class TransitionPathProbability(nn.Cell):
    def __init__(self,
        prob_net,
    ):
        super().__init__()

        self.prob_net = prob_net

        self.concat = P.Concat(axis=-1)
        self.erfc = P.Erfc()

    def construct(self,r,v):
        # [B,2]
        qf = self.concat((r,v))
        qb = self.concat((r,-v))

        # [B,1]
        pf = 0.5 * self.erfc(-self.prob_net(qf))
        pb = 0.5 * self.erfc(-self.prob_net(qb))

        ptp = pf * (1 - pb) + (1.0 - pf) * pb

        return ptp,pf,pb

class TransitionPathProbabilityForAnalysis(nn.Cell):
    def __init__(self,
        space_net,
        prob_net,
    ):
        super().__init__()

        self.space_net = space_net
        self.ptp_net = TransitionPathProbability(prob_net)

        self.reduce_sum = P.ReduceSum()

    def construct(self,R,V):
        r,v = self.space_net(R,V)

        ptp,pf,pb = self.ptp_net(r,v)

        return ptp,r,v

class TransitionPathProbabilityForTrain(nn.Cell):
    def __init__(self,
        space_net,
        prob_net,
        eps=1e-4,
    ):
        super().__init__()

        self.space_net = space_net
        self.ptp_net = TransitionPathProbability(prob_net)
        self.eps = eps

        self.log = P.Log()
        self.reduce_sum = P.ReduceSum()

    def _calc_likeli(self,pf,pb,Mf,Mb):
        log_1_pf = -self.log(C.clip_by_value(1-pf,self.eps,1))
        log_pf = -self.log(C.clip_by_value(pf,self.eps,1))
        likeli_forw = F.select(Mf,log_1_pf,log_pf)
        likeli_forw = self.reduce_sum(likeli_forw)

        log_1_pb = -self.log(C.clip_by_value(1-pb,self.eps,1))
        log_pb = -self.log(C.clip_by_value(pb,self.eps,1))
        likeli_back = F.select(Mb,log_1_pb,log_pb)
        likeli_back = self.reduce_sum(likeli_back)

        return likeli_forw + likeli_back

    def construct(self,R,V,Mf,Mb):
        r,v = self.space_net(R,V)

        ptp,pf,pb = self.ptp_net(r,v)

        likeli = self._calc_likeli(pf,pb,Mf,Mb)

        return ptp,likeli

class TransitionPathProbabilityForMemset(nn.Cell):
    def __init__(self,
        space_mem,
        prob_mem,
    ):
        super().__init__()

        self.space_mem = space_mem
        self.ptp_mem = TransitionPathProbability(prob_mem)

    def construct(self,R,V):
        r,v = self.space_mem(R,V)

        ptp,pf,pb = self.ptp_mem(r,v)

        return ptp

class AlphaChemWithMemorySet(nn.Cell):
    def __init__(self,
        coord_net,
        prob_net,
        batch_size,
        num_contrast,
        weight_positive=0.4,
        likeli_ratio=0.032,
        eps=1e-4,
        momentum_decay=1e-3,
        R_pos_init=None,
        V_pos_init=None,
        R_neg_init=None,
        V_neg_init=None,
        # loss_fn
    ):
        super().__init__(auto_prefix=False)
        self.coord_net = coord_net
        self.prob_net = prob_net
        # self._loss_fn = loss_fn

        self.likeli_ratio = likeli_ratio

        self.momentum_decay = momentum_decay
        self.momentum_coef = 1.0 - momentum_decay

        self.coord_mem = None
        self.prob_mem = None
        self.network_params = None
        self.memset_params = None

        self.batch_size = batch_size
        self.num_contrast = num_contrast
        
        self.coord_mem = copy.deepcopy(coord_net)
        self.prob_mem = copy.deepcopy(prob_net)

        self.space_net = ReducedSpace2D(self.coord_net)
        self.space_mem = ReducedSpace2D(self.coord_mem)

        self.eps = eps

        self.ptp_net = TransitionPathProbabilityForTrain(self.space_net,self.prob_net,self.eps)
        self.ptp_mem = TransitionPathProbabilityForMemset(self.space_mem,self.prob_mem)

        for m,n in zip(self.ptp_mem.get_parameters(),self.ptp_net.get_parameters()):
            m.requires_grad = n.requires_grad

        self.network_params = self.ptp_net.trainable_params()
        self.memset_params = self.ptp_mem.trainable_params()

        for p in self.memset_params:
            p.requires_grad = False

        # [C,B,1]
        self.memset_pos = ms.Parameter(msnp.ones((num_contrast,batch_size,1),ms.float32),name='memset_pos',requires_grad=False)
        self.memset_neg = ms.Parameter(msnp.zeros((num_contrast,batch_size,1),ms.float32),name='memset_neg',requires_grad=False)

        self.assign = P.Assign()
        self.grad = C.GradOperation()
        self.concat = P.Concat(axis=-1)

        if R_pos_init is not None and V_pos_init is not None:
            memset_pos = self._calc_memset(R_pos_init,V_pos_init)
            self.assign(self.memset_pos,memset_pos)

        if R_neg_init is not None and V_neg_init is not None:
            memset_neg = self._calc_memset(R_neg_init,V_neg_init)
            self.assign(self.memset_neg,memset_neg)

        self.weight_positive = weight_positive

        self.log = P.Log()
        self.keep_sum = P.ReduceSum(keep_dims=True)
        self.reduce_sum = P.ReduceSum(keep_dims=False)
        self.keep_mean = P.ReduceMean(keep_dims=True)
        self.reduce_mean = P.ReduceMean(keep_dims=False)

        self.scalar_summary = P.ScalarSummary()
        self.tensor_summary = P.TensorSummary()

        self.count = ms.Parameter(Tensor(0,ms.int32),name='count',requires_grad=False)
        self.one = Tensor(1,ms.float32)
        
        self.print = P.Print()
        self.mod = P.Mod()

    def _update_memset_networks(self):
        for n,m in zip(self.network_params,self.memset_params):
            m = self.momentum_coef * m + self.momentum_decay * n
        return self

    def _calc_memset(self,R,V):
        # [C,B,A,3]
        shape = (self.num_contrast,self.batch_size) + R.shape[-2:]
        R = F.reshape(R,shape)
        V = F.reshape(V,shape)
        memset = []
        for p,v in zip(R,V):
            # [B,1]
            memset.append(self.ptp_mem(p,v))
        # [C,B,1]
        return F.stack(memset)

    def construct(self, Rp, Vp, Mp, Rn, Vn, Mn):
        """Compute the loss function of the AlphaChem.

        Args:
            Rp     (mindspore.Tensor[float], [B*C, A, 3]):    Cartesian coordinates for each atom of positive samples.
            Vp     (mindspore.Tensor[float], [B*C, A, 3]):    Velocities for each atom of positive sample.
            Mp     (mindspore.Tensor[bool],  [B*C, 1]):       Mask for shooting results (True for A and False for B) of positive sample.
            Rn     (mindspore.Tensor[float], [B*C, A, 3]):    Cartesian coordinates for each atom of negative sample.
            Vn     (mindspore.Tensor[float], [B*C, A, 3]):    Velocities for each atom of negative sample.
            Mn     (mindspore.Tensor[bool],  [B*C, 1]):       Mask for shooting results (True for A and False for B) of negative sample.
            
            B:  Batch size
            C:  Number of contrastive samples
            A:  Number of input atoms

        Returns:
            loss mindspore.Tensor[float], [B,1]: loss function of AlphaChem
 
        """

        count = self.mod(self.count + 1,self.num_contrast)
        count = self.assign(self.count,count)
        # print(self.count)

        self._update_memset_networks()

        # [B,1], [B,1], [,]
        ptp_pos,likeli_pos = self.ptp_net(Rp,Vp,Mp,F.logical_not(Mp))
        ptp_neg,likeli_neg = self.ptp_net(Rn,Vn,Mn,Mn)

        # [B,1]
        mem_pos = self.ptp_mem(Rp,Vp)
        mem_neg = self.ptp_mem(Rn,Vn)

        # [C,B,1]
        self.memset_pos[count] = mem_pos
        self.memset_neg[count] = mem_neg

        # [C,B,1] -> [B,1]
        sum_pos = self.reduce_mean(self.memset_pos,0)
        sum_neg = self.reduce_mean(self.memset_neg,0)

        # [B,1]
        loss_pos = -self.log(C.clip_by_value(ptp_pos /(ptp_pos + sum_neg),self.eps,1))
        loss_neg = -self.log(C.clip_by_value((1.-ptp_neg) /((1.-ptp_neg) + (1.-sum_pos)),self.eps,1))

        loss_pos = self.reduce_mean(loss_pos)
        loss_neg = self.reduce_mean(loss_neg)

        loss_contrast = 0.5 * (loss_pos + loss_neg)

        loss_likelihood = self.likeli_ratio * self.weight_positive * likeli_pos + likeli_neg

        self.scalar_summary('loss_contrast',loss_contrast)
        self.scalar_summary('loss_likelihood',loss_likelihood)

        # print(loss_contrast,loss_likelihood)

        # [B,1] = [B,1] + [1,1]
        loss =  loss_contrast + loss_likelihood 

        return loss
